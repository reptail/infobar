InfoBar
==================

AppBar for Windows containing system monitoring (CPU-, MEM-, Network usage, Uptime, etc.).

Developed using C# & WinForms. Originally started on .NET 2 and have upgraded since, currently compiling as .NET 4.0.

There will be no more development on this project by me. 
I've started a new project to upgrade this to WPF instead of WinForms. 
This project used a lot of third party libraries and code, which makes it a bit buggy at times, and hard to debug. 
It is one of the reasons I've started a new project to upgrade to newer technologies.

The new project can be found here: https://bitbucket.org/reptail/infobar2